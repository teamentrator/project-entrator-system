package com.cmwstudios.entrator.admin;

import com.cmwstudios.entrator.system.EntratorSystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KickCommand implements CommandExecutor {

    StringBuilder reason = new StringBuilder();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
            Player player = (Player) sender;

            if (!sender.hasPermission("com.cmwstudios.entrator.admin.KickPlayers")) {

                try {
                    EntratorSystem.throwError(403, sender, null);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return true;
            }
        if (args[0].equals(null)) {
            sender.sendMessage("&4I do apologize, but I must argue, who am I to kick?");
            return false;
        }
        if (args[1].equals(null)) {
            sender.sendMessage("&4I do apologize, but I must argue, why am I kicking them?");
            return false;
        }
            Player player1 = Bukkit.getPlayer(args[0]);
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i] + " ");

        }
            player1.kickPlayer(ChatColor.RED + "You have been kicked from the server because: " + reason.toString());
            reason = new StringBuilder();
        return true;
    }

}
