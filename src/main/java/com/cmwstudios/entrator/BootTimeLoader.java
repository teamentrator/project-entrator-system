package com.cmwstudios.entrator;

import com.cmwstudios.entrator.ChatEvent.ChatEvent;
import com.cmwstudios.entrator.admin.AdminSystem;
import com.cmwstudios.entrator.admin.KickCommand;
import com.cmwstudios.entrator.pizza.*;
import com.cmwstudios.entrator.playerinteractions.PrivateMessaging;
import com.cmwstudios.entrator.secure.EntratorSecure;
import com.cmwstudios.entrator.system.EntratorSystem;
import com.cmwstudios.entrator.system.PlayerJoinEventListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class BootTimeLoader extends JavaPlugin implements EntratorSecure {
    @Override
    public final FileConfiguration getConfig()
    {
        return super.getConfig();
    }
    @Override
    public void onEnable() {
        System.out.println("Your Build is " + buildType);
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new ChatEvent(), this);
        pm.registerEvents(new PlayerJoinEventListener(),this);
        try {
            getCommand("gm").setExecutor(new gm());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("kick").setExecutor(new KickCommand());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("asys").setExecutor(new AdminSystem(this));
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("pm").setExecutor(new PrivateMessaging());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("gma").setExecutor(new gma());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("gmc").setExecutor(new gmc());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("gm").setExecutor(new gms());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        try {
            getCommand("gm").setExecutor(new gmsp());
        } catch (NullPointerException e) {
            EntratorSystem.rtox(e);
        }
        // It sure is boring around here. I just wonder what Ganon's up to!
        PlayerJoinEventListener.joinCounter = 0;
        createConfig();
        System.out.println("Hi. Welcome to Project Entrator. You are on Version 0.1 Revision 3");
    }
    @Override
    public void onDisable() {
        EntratorSystem.stopPlugin();
        System.out.println("Goodbye, and thank you for using Project Entrator.");
    }
    private void createConfig() {
        try {
            if (!getDataFolder().exists()) {
                getDataFolder().mkdirs();
            }
            File file = new File(getDataFolder(), "config.yml");
            if (!file.exists()) {
                getLogger().info("Config.yml not found, creating!");
                saveDefaultConfig();
            } else {
                getLogger().info("Config.yml found, loading!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
