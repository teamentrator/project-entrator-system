package com.cmwstudios.entrator.secure;

public enum BuildType {
    DEVELOPER, PUBLICALPHA, BETATEST, FINALTEST, DEBUG, PRODUCTION, SECURE
}
