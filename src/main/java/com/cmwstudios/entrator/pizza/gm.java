package com.cmwstudios.entrator.pizza;

import com.cmwstudios.entrator.system.EntratorSystem;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class gm implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player player = (Player) sender;
        if (!player.hasPermission("com.cmwstudios.entrator.pizza.SetGameMode")) {
            try {
                EntratorSystem.throwError(403, sender, null);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
            if (args[0].equals(null)) {
                return false;
            }
        try {
            if (!args[1].equals(null)) {
                player = Bukkit.getPlayer(args[1]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {

        }
            if (args[0].equalsIgnoreCase("c")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Creative"));
                player.setGameMode(GameMode.CREATIVE);
            } else if (args[0].equalsIgnoreCase("1")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Creative"));
                player.setGameMode(GameMode.CREATIVE);
            } else if (args[0].equalsIgnoreCase("creative")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Creative"));
                player.setGameMode(GameMode.CREATIVE);
            } else if (args[0].equalsIgnoreCase("s")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Survival"));
                player.setGameMode(GameMode.SURVIVAL);
            } else if (args[0].equalsIgnoreCase("0")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Survival"));
                player.setGameMode(GameMode.SURVIVAL);
            } else if (args[0].equalsIgnoreCase("survival")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Survival"));
                player.setGameMode(GameMode.SURVIVAL);
            } else if (args[0].equalsIgnoreCase("a")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Adventure"));
                player.setGameMode(GameMode.ADVENTURE);
            } else if (args[0].equalsIgnoreCase("2")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Adventure"));
                player.setGameMode(GameMode.ADVENTURE);
            } else if (args[0].equalsIgnoreCase("adventure")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Adventure"));
                player.setGameMode(GameMode.ADVENTURE);
            } else if (args[0].equalsIgnoreCase("sp")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Spectator"));
                player.setGameMode(GameMode.SPECTATOR);
            } else if (args[0].equalsIgnoreCase("3")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Spectator"));
                player.setGameMode(GameMode.SPECTATOR);
            } else if (args[0].equalsIgnoreCase("spectator")) {
                player.sendMessage (ChatColor.translateAlternateColorCodes('&', "&3Your Gamemode was set to Spectator"));
                player.setGameMode(GameMode.SPECTATOR);
            } else {
                return false;
            }
        return true;
    }
}
