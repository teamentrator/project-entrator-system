package com.cmwstudios.entrator.system;

import com.cmwstudios.entrator.admin.AdminSystem;
import com.cmwstudios.entrator.justsoyouknow.JSYN;
import com.cmwstudios.entrator.secure.BuildType;
import com.cmwstudios.entrator.secure.EntratorSecure;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EntratorSystem implements EntratorSecure {
	public EntratorSystem() {
		System.out.println("[Project Entrator System] Welcome to Project Entrator System. A new instance has been started.");
	}
	public static int runningJobs = 0;
	public static boolean stopping = false; // Tells classes to find a stopping place
	public static boolean stopped = false; // Tells stopPlugin() whether the class is finished with everything
	public static void rtox(Exception e) {
		System.out.println("A serious error occurred that Project Entrator could not recover from. Project Entrator is unable to continue normal operation");
		System.out.println("Se produjo un error grave que Project Entrator no puede recuperar. Project Entrator no puede continuar.");
		System.out.println("Une erreur grave est survenue lors de la recuperation du projet. L'entree de projet ne peut pas continuer.");
		System.out.println("E schlemm Feeler ass opgetrueden datt de Projet Entrator net erliichtert vu. Project Entrator kann net veiderfueren.");
		System.out.println("Ett allvarligt fel intraffade som Project Entrator inte kan aterhamta sig fran. Projecet Entrator kan inte fortsatta.");
		System.out.println("Due to the severity of the error, the stack trace will now display.");
		e.printStackTrace();
        if (buildType == BuildType.SECURE) {
            System.out.println("You are running a secure build, but errors like this shouldn't occur on a secure build as per standard. Your secure build has most likely have been compromised or corrupted. For more information, visit (TODO: insert URL)");
            System.out.println("As such, the server will cease to function until the error is corrected.");
            Bukkit.getServer().shutdown();
        }
	}
	public static void failure(int code, String classname) {
		runningJobs++;
		if (stopping) {
			runningJobs--;
			if (runningJobs == 0) {
				stopped = true;
			}
			return;
		}
		Bukkit.broadcastMessage("[PE System] An error occured.");
		Bukkit.broadcastMessage("Error:" + code);
		Bukkit.broadcastMessage("At: " + classname);
		Bukkit.broadcastMessage(ChatColor.DARK_RED + "Project Entrator cannot continue. System Halted.");
		stopPlugin();
		runningJobs--;
	}
	public static void messagePlayer(Player player, String message) {
		runningJobs++;
		if (stopping) {
			runningJobs--;
			if (runningJobs == 0) {
				stopped = true;
			}
			return;
		}
		player.sendMessage(ChatColor.BLUE + "[PE] " + message);
		runningJobs--;
	}
	public static void throwError(int ErrorType, CommandSender sender, String details) throws InterruptedException {

		Player player = (Player) sender;
		if (ErrorType == 403) {
            JSYN.notifyUser(player, "&4You don't have permission", "&1Entrator System", false);
		} else if (ErrorType <=299) {
			sender.sendMessage(ChatColor.GREEN + "The operation completed successfully. Application returned HTTP code: " + ErrorType);
		} else if (ErrorType == 600) {
			sender.sendMessage(ChatColor.GOLD + "This message shouldn't appear. If it does, it's a bug, so please tell me right away. Debugging information will now display.");
			sender.sendMessage("The command, function, or boolean called was valid, but goes unused and should not be executable in the final product.");
			sender.sendMessage("Additional debug information: " + details);
			try {
      throw new RuntimeException("Unused Function Called");
    } catch (Exception e) {

    }

		} else if (ErrorType == 704) {
			throwError(600, sender, details);
			sender.sendMessage("DEBUG: Invalid Call Sign, Byte 2 should always be x. Avoid using the debug console if you don't know what you're doing.");
		}
	}

	public static void throwGlobalError() throws InterruptedException {
		throwError(600, null, null);
	}
	public static void broadcastMessage(String message) {
		runningJobs++;
		if (stopping) {
			runningJobs--;
			if (runningJobs == 0) {
				stopped = true;
			}
			return;
		}
		Bukkit.broadcastMessage(ChatColor.GREEN + "[PE] " + message);
		runningJobs--;
	}
	public static void stopPlugin() {
		stopping = true;
		System.out.println("Waiting for Entrator System (com.cmwstudios.entrator.system.EntratorSystem) to stop...");
		while(!stopped) {
			if (runningJobs == 0) {
				break;
			}
		}
		System.out.println("Waiting for the Admin System (com.cmwstudios.entrator.admin.AdminSystem) to stop.");
		while (!AdminSystem.stopped) {
			if (AdminSystem.runningJobs == 0) {
				break;
			}
		}

	}
}
