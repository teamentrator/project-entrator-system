package com.cmwstudios.entrator.justsoyouknow;

import me.confuser.barapi.BarAPI;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.entity.Player;

public class JSYN  {
    public static void notifyUser(Player player, String message, String application, Boolean persistant) throws InterruptedException {
        if (persistant) {
            while (true) {
                player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.G));
                Thread.sleep(100, 0);
                player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.F));
                Thread.sleep(100, 0);
                player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.G));
                BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&8[&3" + application + "&8] " + "&2" + message));
            }
        } else {
            player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.G));
            Thread.sleep(100, 0);
            player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.F));
            Thread.sleep(100, 0);
            player.playNote(player.getLocation(), Instrument.XYLOPHONE, Note.natural(1, Note.Tone.G));
            BarAPI.setMessage(player, ChatColor.translateAlternateColorCodes('&', "&8[&3" + application + "&8] " + "&2" + message), 10);
        }
    }

    public static void notifyServer(String message, String application, Boolean persistant) {
        if (persistant) {
            while (true) {

                BarAPI.setMessage(ChatColor.translateAlternateColorCodes('&', "&8[&3" + application + "&8] " + "&2" + message));
            }
        } else {
            BarAPI.setMessage(ChatColor.translateAlternateColorCodes('&', "&8[&3" + application + "&8] " + "&2" + message), 10);
        }
    }



}