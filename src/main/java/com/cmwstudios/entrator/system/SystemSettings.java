package com.cmwstudios.entrator.system;

import com.cmwstudios.entrator.BootTimeLoader;

public class SystemSettings {
    private static SystemSettings systemSettings;
    BootTimeLoader plugin;

    public SystemSettings(BootTimeLoader instance) {
        plugin = instance;
        System.out.println("[System Settings] New instance successfully started.");
    }
    public String getDoNotDisturbStatus() {
        if (plugin.getConfig().getBoolean("server.configuration.do-not-disturb.enabled")) {
            return plugin.getConfig().getString("server.configuration.do-not-disturb.level");
        } else return "false";
    }
    public static boolean staticGetPriorityOnlyAllows(String publicprivate, String from) {
        return systemSettings.getPriorityOnlyAllows(publicprivate, from);
    }
    public static String staticGetDoNotDisturbStatus() {
        return systemSettings.getDoNotDisturbStatus();
    }
    private boolean getPriorityOnlyAllows(String publicprivate, String from) {
        if (publicprivate == "private") {
            if (from == "system") {
                return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.private.system");
            } else if (from == "anyone") {
                return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.private.anyone");
            } else if (from == "operators") {
                return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.private.operators");
            } else if (from == "permission-dndspeak") {
                return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.private.permission-dndspeak");
            }
            if (publicprivate == "public") {
                if (from == "system") {
                    return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.public.system");
                } else if (from == "anyone") {
                    return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.public.anyone");
                } else if (from == "operators") {
                    return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.public.operators");
                } else if (from == "permission-dndspeak") {
                    return plugin.getConfig().getBoolean("server.configuration.do-not-disturb.priority-only.permit.public.permission-dndspeak");
                }

            }
        }
        return false;
    }
}