package com.cmwstudios.entrator.admin;

import com.cmwstudios.entrator.BootTimeLoader;
import com.cmwstudios.entrator.system.EntratorSystem;
import com.jonodonozym.UEconomy.UEconomyAPI;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AdminSystem implements CommandExecutor {
	BootTimeLoader plugin;

	public AdminSystem(BootTimeLoader instance) {
		plugin = instance;
	}
	public static int runningJobs = 0;
	public static boolean stopped = false;
	Object players[];
	String reason;
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		runningJobs++;
		if (EntratorSystem.stopping) {
			runningJobs--;
			if (runningJobs == 0) {
				stopped = true;
				return true;
			}
		}
		double cost = plugin.getConfig().getDouble("commmands.asys.cost");
		if (cost < 0) {
			cost = 0;
		} else if (cost > 0) {
			UEconomyAPI.subBalance(sender.getName(), cost);
		}
		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Stop trying to cause a ArrayIndexOutOfBounds Exception, use arguments!");
			runningJobs--;
			if (runningJobs == 0) {
				stopped = true;
			}
			return false;
		}
    		if (args[0].equalsIgnoreCase("version")) {
    			sender.sendMessage(ChatColor.DARK_PURPLE + "[ASys] You are on version 0.2 Dependant. Thank you for using ASys and Project Entrator.");
    			runningJobs--;
				if (runningJobs == 0) {
					stopped = true;
				}
    			return true;
    		 }

    runningJobs--;
		if (runningJobs == 0) {
			stopped = true;
		}
    return true;
    }

}
