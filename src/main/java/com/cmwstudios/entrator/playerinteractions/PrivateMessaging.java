package com.cmwstudios.entrator.playerinteractions;

import com.cmwstudios.entrator.justsoyouknow.JSYN;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PrivateMessaging implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        Player player = (Player) sender;
        if  (args[0].equals(null)) {
            sender.sendMessage("&4You should tell me who you want to PM.");
            return false;
        } if (args[1].equals(null)) {
            sender.sendMessage("&4You should tell me a message to send.");
            return false;
        }
        StringBuilder message = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            message.append(args[i] + " ");
        }

        Player sendee = Bukkit.getPlayer(args[0]);
        try {
            JSYN.notifyUser(sendee, message.toString(), "&2" + sender.getName() + " → You", false);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Let's hope this doesn't blow up in my face. Feel free to uncomment the line below if you want. OwO
        // sendee.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2" + sender.getName() + " → You&8] &2" + message.toString()));
        return true;
    }
}