package com.cmwstudios.entrator.system;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEventListener implements Listener {


    public static int joinCounter = 0;
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        {

            Player p = event.getPlayer();
            String playerName = p.getName();
            String playerUDID = p.getUniqueId().toString();



            joinCounter++;
            event.setJoinMessage(ChatColor.GREEN + "Oh, Hi, " + ChatColor.AQUA + p.getDisplayName());

            Bukkit.broadcastMessage(ChatColor.GOLD + "Since the last restart, this is how many players have joined: " + joinCounter);

        }
    }

}

