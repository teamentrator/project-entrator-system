package com.cmwstudios.entrator.pizza;

import com.cmwstudios.entrator.system.EntratorSystem;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class gms implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!sender.hasPermission("com.cmwstudios.entrator.pizza.SetGameMode")) {
            try {
                EntratorSystem.throwError(403, sender, null);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        }
        Player player = (Player) sender;
        player.setGameMode(GameMode.SURVIVAL);
        return false;
    }
}
